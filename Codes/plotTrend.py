import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import metapy
import pandas as pd
import sys
import seaborn as sns

import shutil
import os
import numpy as np
import math
from scipy import interpolate
import datetime


metapy.log_to_stderr()

K=15

InputDirectory='/Users/shubhrakanti/Documents/DTG/Figures/TopicData/'
OutputDirectory='/Users/shubhrakanti/Documents/DTG/Figures/'

#Influential='all'
#Influenced='kdd'

InfluenceFlag='output_'+'kdd'

try:
    shutil.rmtree(OutputDirectory+'TopicTrends')
    shutil.rmtree(OutputDirectory+'SwarmPlots')
    shutil.rmtree(OutputDirectory+'PaperCountDistribution')
except:
    print('Output Directory not present')



os.mkdir(OutputDirectory+'TopicTrends')
os.mkdir(OutputDirectory+'SwarmPlots')
os.mkdir(OutputDirectory+'PaperCountDistribution')




TopicTrend={}
PaperCountDistributions={}


for i in range(0, K):
    TopicTrend[i+1]={}

with open(InputDirectory+'topicDistribution.'+InfluenceFlag+'.txt','r',encoding='utf-8') as inputfile:
    for line in inputfile:
        year=int(line.strip().split(',')[0])+1996

        probs=list(map(float,line.strip().split(',')[1:-1]))
        for i in range(len(probs)):
            TopicTrend[i+1][year]=probs[i]
                

for i in range(0, K):
    proportions=list(TopicTrend[i+1].values())
    mymean=np.mean(proportions)
    mystd=max(2*np.std(proportions),0.05)

    print(i+1, mymean, mystd)

    for year in TopicTrend[i+1]:
        if TopicTrend[i+1][year]> mymean+mystd or TopicTrend[i+1][year]< mymean-mystd:
            try:
                TopicTrend[i+1][year]=np.mean([TopicTrend[i+1][year-1],TopicTrend[i+1][year+1]])
            except:
                TopicTrend[i+1][year]=mymean


for i in range(K):
    lists = sorted(TopicTrend[i+1].items()) # sorted by key, return a list of tuples

    x, y = zip(*lists) # unpack a list of pairs into two tuples
    
    x=list(map(int,x))
    x=list(map(str,x))

    plt.figure()
    ax = plt.subplot(111)
    ax.plot(x, y, 'g--',x, y, 'go',markersize=12)
    ax.locator_params(integer=True)
    
    box = ax.get_position()
    ax.set_position([box.x0, box.y0 + box.height * 0.1, box.width, box.height * 0.9])

    axes = plt.gca()
    #axes.set_ylim([0.0,0.3])
    ax.tick_params(axis='both', which='major', labelsize=15)
    ax.tick_params(axis='both', which='minor', labelsize=15)
    
    plt.savefig(OutputDirectory+"TopicTrends/Topic-{}-".format(i + 1)+InfluenceFlag+'.png',bbox_inches='tight')
    plt.close()
    
    
