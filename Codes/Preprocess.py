


DataDirectory='/Users/shubhrakanti/Documents/DTG/RawData/'
OutputDirectory='/Users/shubhrakanti/Documents/DTG/Data/'

Papers={}
with open (DataDirectory+'ML.txt', encoding='utf-8') as inputfile:
    for line in inputfile:
        title=line.split('\t')[4]
        year=line.split('\t')[6]
        conference=line.split('\t')[11].strip()

        if conference not in Papers:
            Papers[conference]={}

        if year not in Papers[conference]:
            Papers[conference][year]=[]

        Papers[conference][year].append(title)


with open (OutputDirectory+'all.dat', 'w', encoding='utf-8') as allfile:
    for conference in Papers:
        with open (OutputDirectory+conference+'.dat', 'w', encoding='utf-8') as outputfile:
            for year in sorted(Papers[conference]):
                for title in Papers[conference][year]:
                    outputfile.write(year+'\t'+title.strip()+'\n')
                    allfile.write(year+'\t'+title.strip()+'\n')
