#!/usr/bin/env bash

rm Results.txt
touch Results.txt

python3 SQTP.py
python3 test.py SQTP

python3 LSTM.py  
python3 test.py LSTM  


python3 Random.py  
python3 test.py Random  


python3 Posterior.py 
python3 test.py Posterior  
