import metapy
import pandas as pd
import sys
import seaborn as sns
import matplotlib.pyplot as plt
import shutil
import os
import numpy as np
import math
from scipy import interpolate
import datetime


metapy.log_to_stderr()



TopicModelDirectory='/Users/shubhrakanti/Documents/DTG/Data/'
InputDirectory='/Users/shubhrakanti/Documents/DTG/GeneratedText/'
OutputDirectory='/Users/shubhrakanti/Documents/DTG/Figures/TopicData/'
FigureDirectory='/Users/shubhrakanti/Documents/DTG/Figures/'



InfluenceFlag='output_'+'sigir'

try:
    shutil.rmtree(OutputDirectory)
except:
    print('Output Directory not present')

try:
    shutil.rmtree(FigureDirectory)
except:
    print('Figure Directory not present')
    

os.mkdir(FigureDirectory)
os.mkdir(OutputDirectory)
os.mkdir(FigureDirectory+'TopicTrends')
os.mkdir(FigureDirectory+'SwarmPlots')
os.mkdir(FigureDirectory+'PaperCountDistribution')


fidx = metapy.index.make_forward_index(TopicModelDirectory+'pair.toml')
model = metapy.topics.TopicModel(TopicModelDirectory+'lda-pgibbs-pair')


with open(OutputDirectory+'topics.txt','w') as outputfile:
    for topic in range(0, model.num_topics()):
        outputfile.write("\n\n\nTopic {}:\n".format(topic + 1))
        for tid, val in model.top_k(topic, 10, metapy.topics.BLTermScorer(model)):
            outputfile.write(("{}: {}\n".format(fidx.term_text(tid), val)))
        outputfile.write("======\n")


TopicTrend={}
PaperCountDistributions={}


for i in range(0, model.num_topics()):
    TopicTrend[i+1]={}

with open(InputDirectory+InfluenceFlag+'.txt','r',encoding='utf-8') as inputfile:
    for line in inputfile:
        year=int(line.strip().split('\t')[0])+1996
        print('Inferring for year: '+str(year))

        if year not in PaperCountDistributions:
            PaperCountDistributions[year]=0
        PaperCountDistributions[year]+=1

        
        doc = metapy.index.Document()         
        doc.content(line.strip().split('\t')[1])
        dvec = fidx.tokenize(doc)
        inferencer = metapy.topics.GibbsInferencer(TopicModelDirectory+'lda-pgibbs-pair.phi.bin', alpha=0.01)
        props = inferencer.infer(dvec, max_iters=100, rng_seed=42)
        
        for i in range(model.num_topics()):
            if year not in TopicTrend[i+1]:
                TopicTrend[i+1][year]=0.0
            TopicTrend[i+1][year]+=props.probability(i)



for topic in TopicTrend:
    for year in TopicTrend[topic]:
        TopicTrend[topic][year]/=PaperCountDistributions[year]



with open(OutputDirectory+'topicDistribution.'+InfluenceFlag+'.txt','w') as outputfile:
    for year in sorted(TopicTrend[1]):
        outputfile.write(str(int(year)-1996)+',')
        for topic in TopicTrend:
            outputfile.write(str(TopicTrend[topic][year])+',')
        outputfile.write('\n')



for i in range(model.num_topics()):
    lists = sorted(TopicTrend[i+1].items()) # sorted by key, return a list of tuples

    x, y = zip(*lists) # unpack a list of pairs into two tuples
    
    x=list(map(int,x))
    x=list(map(str,x))
     
    plt.plot(x, y, '--',x, y, 'bo',markersize=12)
    plt.locator_params(integer=True)
    
    
    plt.savefig(FigureDirectory+"TopicTrends/Topic-{}-".format(i + 1)+InfluenceFlag+'.png')
    plt.close()
    
    
