import sys
import shutil
import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='3'
from os import listdir
from os.path import isfile, join
import numpy
from bisect import bisect
from random import random
from nltk.translate import bleu_score
from nltk.translate.bleu_score import SmoothingFunction
import numpy as np
import tensorflow as tf
from tensorflow.contrib import rnn
import random
from random import uniform,random
import collections
import time
import codecs
import json
import math



def isEnglish(s):
    try:
        codecs.encode(s,'ascii')
    except:
        return False
    else:
        return True
    
tf.logging.set_verbosity(tf.logging.ERROR)
#Infleuntial='kdd'
InfluencedList=['kdd','nips','sigir','icml','www','cvpr']
print('\nThis is Posterior.py')


for Influenced in InfluencedList:
    print('Training: '+ Influenced)
    
    DataDirectory='/datadrive/Santu/Influence/Data/'+Influenced+'/'
    embedFile = '/datadrive/Santu/Influence/Data/glove.6B.100d.txt'

    Stopwords=[]

    with open('stop-word-list.txt','r')as inputfile:
        for line in inputfile:
            Stopwords.append(line.strip().lower())
            


     
    start_time = time.time()
    def elapsed(sec):
        if sec<60:
            return str(sec) + " sec"
        elif sec<(60*60):
            return str(sec/60) + " min"
        else:
            return str(sec/(60*60)) + " hr"
        



    def prepare_data(content):
        content = np.array(content)
        content = np.reshape(content, [-1, ])
        return content





    def sample(preds, temperature=1.0):
        # helper function to sample an index from a probability array
        preds = np.asarray(preds).astype('float64')
        preds = np.log(preds) / temperature
        exp_preds = np.exp(preds)
        preds = exp_preds / np.sum(exp_preds)
        probas = np.random.multinomial(1, preds, 1)
        return np.argmax(probas)




    def build_dataset(words,confs):
        count = collections.Counter(words).most_common()
        dictionary = dict()

        dictionary['$emp$']=len(dictionary)
        #dictionary['<unk>']=len(dictionary)


        for confname in confs:
            conf=confname.replace('.txt','')
            dictionary['<unk_'+conf+'>']=len(dictionary)
            #Stopwords.append('<unk_'+conf+'>')

        
        for word,wordcount in count:
            '''if word.isdigit()==True or (len(word)<2 and word not in Stopwords):
                continue'''
            if wordcount >= 3:
                dictionary[word] = len(dictionary)
        
        reverse_dictionary = dict(zip(dictionary.values(), dictionary.keys()))

        return dictionary, reverse_dictionary




    def loadGloVe(embedFile):
        embeddingsDict={}
        
        file = open(embedFile,'r', encoding="utf8")
        #print('Loading Embeddings!')
        
        for line in file.readlines():
            if line.split()[0] in dictionary:
                row = line.strip().split(' ')
                embeddingsDict[row[0]]=np.asarray(row[1:])
                #embeddingsDict[row[0]]=np.asarray([dictionary[line.split()[0]]])
                #vocab.append(row[0])
                #embd.append(row[1:])
        file.close()

        embeddingsArray=[]

        with open('../Models/'+Influenced+'/Unknown_Word_Embeddings.txt','w') as MyOutputfile:
            for wordid in sorted(reverse_dictionary):
                if reverse_dictionary[wordid] not in embeddingsDict:
                    #embeddingsDict[reverse_dictionary[wordid]]=np.asarray([0.0 for i in range(len(row[1:]))])
                    if reverse_dictionary[wordid]=='$emp$':
                        embeddingsDict[reverse_dictionary[wordid]]=numpy.zeros(len(row[1:]))
                    else:
                        embeddingsDict[reverse_dictionary[wordid]]=numpy.random.uniform(-1, 1, size=len(row[1:]))
                    MyOutputfile.write(reverse_dictionary[wordid])
                    for number in embeddingsDict[reverse_dictionary[wordid]]:
                        MyOutputfile.write(' '+str(number))
                    MyOutputfile.write('\n')
                embeddingsArray.append(embeddingsDict[reverse_dictionary[wordid]])
                
                
                
        #print('Embeddings Loaded!')
        
        return embeddingsDict,embeddingsArray





    class ToySequenceData(object):   
        def __init__(self, raw_data, YearToi, embeddingsDict, embedding_dim, r, max_seq_len=50):
            self.dataSize=len(raw_data)
            self.batch_id = 0

                                                  
        def next(self, batch_size, raw_data, YearToi, embeddingsDict, embedding_dim, r, max_seq_len=50):                                                       
            if self.batch_id == self.dataSize:
                self.batch_id = 0

            batch_data=[]
            batch_labels=[]
            batch_seqlen=[]
            batch_year=[]
            #batch_seqBagofWords=[]

            
            
            for i in range(self.batch_id,min(self.batch_id +batch_size, self.dataSize)):
                for j in range(1,len(raw_data[i][1])):
                    batch_seqlen.append(j)
                    s=[]
                    for k in range(j):
                        s.append(dictionary[raw_data[i][1][k]])
                    s+= [dictionary['$emp$'] for Myi in range(max_seq_len - j)]

                    #print(raw_data[i][1][:j],raw_data[i][1][j])

                    #print(raw_data[i])
                    batch_data.append(s)
                    
                    symbols_out_onehot = np.zeros([vocab_size], dtype=float)
                    symbols_out_onehot[dictionary[raw_data[i][1][j]]] = 1.0
                    batch_labels.append(symbols_out_onehot)

                    batch_year.append(YearToi[raw_data[i][0]])
                    
                    '''frCount=np.zeros([vocab_size], dtype=float)

                    Mycount = collections.Counter(raw_data[i][1][:j]).most_common()
                    for word,wordcount in Mycount:
                        if word != raw_data[i][1][j]:
                            frCount[dictionary[word]]+=1.0

                    MySum=sum(frCount)
                    if MySum!=0:
                        frCount=frCount/MySum
                    batch_seqBagofWords.append(frCount)'''

            #print('Processing Tweet: '+str(self.batch_id)+'-'+str(min(self.batch_id +batch_size, self.dataSize))
                  #+'\t Sequences='+str(len(batch_data)))
            
            self.batch_id = min(self.batch_id + batch_size, self.dataSize)
            
            return batch_data, batch_labels, batch_seqlen, batch_year, self.batch_id
        
                                                  





    # Parameters
    learning_rate = 0.01
    training_iters = 605
    batch_size = 1800
    display_step = 10
    model_save_step=100

    # number of units in RNN cell
    n_hidden = 128
    n_hidden_1_MLP=128
    n_hidden_2_MLP=128
    n_hidden_Joint=128
    n_topics=15
    n_bias=15

    r=4





        
    '''def Topic_network(RNNresults,seqBagofWords, weights, biases, BM, Mask):
        PI_Layer=tf.matmul(RNNresults, tf.abs(tf.transpose(weights['out_Topic'])))
        out_layer=tf.matmul(PI_Layer, tf.abs(weights['out_Topic']))

        return out_layer'''





    def History_network(seqyear, weights, biases):
        layer_1 = tf.add(tf.matmul(tf.gather(AggregatedInfluencedHistoryData,seqyear),weights['h1_History']), biases['b1_History'])
        layer_1 = tf.nn.relu(layer_1)
        #layer_1=tf.contrib.layers.maxout(layer_1,)

        out_layer = tf.matmul(layer_1, weights['out_History']) +biases['out_History']

        return out_layer



    '''def Topic_network(seqBagofWords, weights, biases, HistoryVector):
        layer_1 = tf.add(tf.matmul(seqBagofWords,weights['h1_Topic']), biases['b1_Topic'])
        layer_1 = tf.nn.relu(layer_1)

        #layer_2 = tf.add(tf.matmul(layer_1, weights['h2_Topic']), biases['b2_Topic'])
        #layer_2 = tf.nn.relu(layer_2)

        PI_Layer=tf.add(tf.matmul(layer_1, weights['out_Topic_PI']),biases['out_Topic_PI'])

        PI_Posterior=PI_Layer + HistoryVector
        
        return PI_Posterior'''



    def Influential_History_network(seqyear, weights, biases):
        layer_1 = tf.add(tf.matmul(tf.gather(AggregatedInfluentialHistoryData,seqyear),weights['h1_Influential_History']), biases['b1_Influential_History'])
        layer_1 = tf.nn.relu(layer_1)

        out_layer = tf.matmul(layer_1, weights['out_Influential_History']) +biases['out_Influential_History']

        return out_layer



    '''def History_network(seqyear, weights, biases):
        layer_1 = tf.add(tf.matmul(tf.gather(HistoryData,seqyear),weights['h1_History']), biases['b1_History'])
        layer_1 = tf.nn.relu(layer_1)

        out_layer = tf.matmul(layer_1, weights['out_History']) +biases['out_History']

        return out_layer'''





    '''def History_network(seqyear, weights, biases):
        layer_1 = tf.add(tf.matmul(tf.gather(HistoryData,seqyear),weights['h1_History']), biases['b1_History'])
        layer_1 = tf.nn.relu(layer_1)

        layer_2 = tf.add(tf.matmul(layer_1, weights['h2_History']), biases['b2_History'])
        layer_2 = tf.nn.relu(layer_2)
        
        out_layer = tf.matmul(layer_2, weights['out_History']) +biases['out_History']

        return out_layer'''





    def dynamicRNN(x, seqlen, weights, biases, V_History, V_Influential_History):
        #V_History=tf.reshape(tf.tile(V_History,[1,maxSeqlength]),[-1,maxSeqlength,n_bias])
        #V_Influential_History=tf.reshape(tf.tile(V_Influential_History,[1,maxSeqlength]),[-1,maxSeqlength,n_bias])

        
        x=tf.nn.embedding_lookup(W, x)
        #x=tf.concat([tf.nn.embedding_lookup(W, x), V_History],axis=2)
        #x=tf.concat([tf.nn.embedding_lookup(W, x),V_History, V_Influential_History],axis=2)
        


        x = tf.unstack(x, maxSeqlength, 1)
        lstm_cell = tf.contrib.rnn.BasicLSTMCell(n_hidden)
        outputs, states = tf.contrib.rnn.static_rnn(lstm_cell, x, dtype=tf.float32, sequence_length=seqlen)
        outputs = tf.stack(outputs)
        outputs = tf.transpose(outputs, [1, 0, 2])

        my_batch_size = tf.shape(outputs)[0]
        #print(batch_size)
        index = tf.range(0, my_batch_size) * maxSeqlength + (seqlen - 1)
        outputs = tf.gather(tf.reshape(outputs, [-1, n_hidden]), index)


        return tf.add(tf.matmul(outputs, weights['out_rnn']),biases['out_rnn'])





    def JointNetwork(x,seqlen,weights,biases,seqyear,alpha, beta):
        V_History=History_network(seqyear, weights, biases)
        #V_Influential_History=Influential_History_network(seqyear, weights, biases)
        V_RNN=dynamicRNN(x, seqlen, weights, biases, None, None)

        #V_RNN2=dynamicRNN(x, seqlen, weights, biases, V_Influential_History, True)

        return alpha*V_RNN + beta*V_History
        #return V_RNN



    YearToi={}
    iToYear={}
    Conference={}


    confs = ['TrainPapers.txt']

    ConfArray=[]
    #confs=['kdd']
    Conference=[]

    AllWords=[]
    BagOfWords={}


    ActualTitles=[]
    paperCount=0
    maxSeqlength=0

    IDF={}


        
    for confname in sorted(confs):
        conf=confname.replace('.txt','')
        ConfArray.append(conf)
        with open(DataDirectory+conf+'.txt','r') as inputfile:
            for line in inputfile:
                year=int(line.split('\t')[0])
                InitialTitle=line.strip().split('\t')[1]
                if isEnglish(InitialTitle)==False:
                    continue
                Title='# '+ InitialTitle + ' .'
                AllWords+=Title.split()
                paperCount+=1

    #print('Total Tweets: ' +str(paperCount))
    dictionary, reverse_dictionary = build_dataset(AllWords,confs)
    vocab_size = len(dictionary)
    #print('Vocabulary Size: ' +str(vocab_size))



    BackGroundModel=[0 for i in range(vocab_size)]
    MaskModel=[0 for i in range(vocab_size)]

    ConfDistribution={}
    ConferenceOneHotVector={}
    for confname in confs:
        conf=confname.replace('.txt','')
        ConferenceOneHotVector[conf]=numpy.zeros(len(ConfArray))
        ConferenceOneHotVector[conf][ConfArray.index(conf)]=1.0
        
        with open(DataDirectory+'/'+conf+'.txt','r') as inputfile:
            for line in inputfile:
                year=int(line.split('\t')[0])
                InitialTitle=line.strip().split('\t')[1]
                if isEnglish(InitialTitle)==False:
                    continue
                

                Title='#'

                for word in InitialTitle.split():
                    if word in dictionary:
                        Title+=' '+word
                    #else:
                        #Title+=' <unk_'+conf+'>'

                Title+=' .'

                if len(Title.split()) < 5:
                    continue

                #print(Title)
                
                for word in set(InitialTitle.split()):
                    if word in dictionary:
                        if word not in IDF:
                            IDF[word]=1
                        else:
                            IDF[word]+=1
                


                ActualTitles.append(InitialTitle)
                
                if year not in BagOfWords:
                    BagOfWords[year]={}

                if conf not in BagOfWords[year]:
                    BagOfWords[year][conf]={}

                for word in Title.split()[1:-1]:
                    if word in BagOfWords[year][conf]:
                        BagOfWords[year][conf][word]+=1
                    else:
                        BagOfWords[year][conf][word]=1

                if year< 4 or year > 20:
                    continue
                
                Conference.append((year,Title.split(),conf))


                for word in Title.split():
                    if word in Stopwords:
                        BackGroundModel[dictionary[word]]+=1
                        MaskModel[dictionary[word]]=1

                if len(Title.split())+1 > maxSeqlength:
                    maxSeqlength=len(Title.split())+1

                
                paperCount+=1

                if conf not in ConfDistribution:
                    ConfDistribution[conf]=[]
                ConfDistribution[conf].append(1)


                                              
    '''print('\n\n\n')
    for Myconf in sorted(ConfDistribution,key=ConfDistribution.get, reverse=True):
        print(Myconf,numpy.sum(ConfDistribution[Myconf]))

    print('\n\n\n')'''
                
    for word in IDF:
        IDF[word]=math.log(1+(float(paperCount)/IDF[word]))




    embeddingsDict,embeddingsArray=loadGloVe(embedFile)
    embedding_dim = len(embeddingsArray[0])

    W = tf.Variable(tf.constant(0.0, shape=[vocab_size, embedding_dim]),trainable=True, name="W")
    embedding_placeholder = tf.placeholder(tf.float32, [vocab_size, embedding_dim])
    embedding_init = W.assign(embedding_placeholder)



    '''HistoryData=[]
    i=0
    YearCounter=0
    for year in sorted(BagOfWords):
        YearToi[year]=YearCounter
        iToYear[YearCounter]=year
        for conf in ConfArray:    
            HistoryData.append([])
            for index in range(1,r+1):
                if YearCounter-index <0:
                    for word in sorted(dictionary,key=dictionary.get):
                        HistoryData[i].append(0.0)
                else:
                    if conf not in BagOfWords[iToYear[YearCounter-index]]:
                        for word in sorted(dictionary,key=dictionary.get):
                            HistoryData[i].append(0.0)
                    else:
                        for word in sorted(dictionary,key=dictionary.get):
                            if word in BagOfWords[iToYear[YearCounter-index]][conf]:
                                MyTF=1+math.log(float(BagOfWords[iToYear[YearCounter-index]][conf][word]))
                                HistoryData[i].append(MyTF*IDF[word])
                            else:
                                HistoryData[i].append(0.0)
            i+=1
        YearCounter+=1


    for i in range(len(HistoryData)):
        for index in range(0,r):
            MySum=sum(HistoryData[i][vocab_size*index : vocab_size*(index+1)])
            if MySum==0.0:
                continue
            for j in range(0,vocab_size):
                HistoryData[i][vocab_size*index+j]=HistoryData[i][vocab_size*index+j]/MySum'''
                


    YearCounter=0
    InfluentialHistoryData=[]
    InfluencedHistoryData=[]

    '''with open(DataDirectory+'topicDistribution.'+Infleuntial+'.txt','r') as inputfile:
        for line in inputfile:
            year=int(line.split('=')[0])
            YearToi[year]=YearCounter
            iToYear[YearCounter]=year

            InfluentialHistoryData.append([])
            distribution=line.split('=')[1].split(',')

            for item in distribution[:-1]:
                #InfluentialHistoryData[YearCounter].append(float(numpy.random.uniform(0,.4,1)[0]))
                InfluentialHistoryData[YearCounter].append(float(item.split(':')[1]))
                
            YearCounter+=1

    #print(InfluentialHistoryData[0])'''

    YearCounter=0
    with open(DataDirectory+'topicDistribution.'+Influenced+'.txt','r') as inputfile:
        for line in inputfile:
            year=int(line.split('=')[0])
            YearToi[year]=YearCounter
            iToYear[YearCounter]=year

            InfluencedHistoryData.append([])
            distribution=line.split('=')[1].split(',')
            
            for item in distribution[:-1]:
                #InfluencedHistoryData[YearCounter].append(float(numpy.random.uniform(0,.4,1)[0]))
                InfluencedHistoryData[YearCounter].append(float(item.split(':')[1]))
            YearCounter+=1
                

    #print(InfluencedHistoryData[0])

    AggregatedInfluentialHistoryData=[]
    AggregatedInfluencedHistoryData=[]

    for year in range(len(InfluentialHistoryData)):
        AggregatedInfluentialHistoryData.append([])
        for i in range(1,r+1):
            if year-i >=0:
                for proportion in InfluentialHistoryData[year-i]:
                    AggregatedInfluentialHistoryData[year].append(proportion)
            else:
                for proportion in InfluentialHistoryData[year-i]:
                    AggregatedInfluentialHistoryData[year].append(0.0)


    for year in range(len(InfluencedHistoryData)):
        AggregatedInfluencedHistoryData.append([])
        for i in range(1,r+1):
            if year-i >=0:
                for proportion in InfluencedHistoryData[year-i]:
                    AggregatedInfluencedHistoryData[year].append(proportion)
            else:
                for proportion in InfluencedHistoryData[year-i]:
                    AggregatedInfluencedHistoryData[year].append(0.0)
                

    training_data = ToySequenceData(Conference, YearToi, embeddingsDict,embedding_dim, r, maxSeqlength)

    #print('Total Sequences: '+ str(len(training_data.data)))
    
    # tf Graph input
    x = tf.placeholder(tf.int32, [None, maxSeqlength],name='x')
    seqBagofWords=tf.placeholder("float", [None, vocab_size],name='seqBagofWords')
    y = tf.placeholder("float", [None, vocab_size],name='y')
    seqlen = tf.placeholder(tf.int32, [None],name='seqlen')
    seqyear=tf.placeholder(tf.int32, [None],name='seqyear')
    BM=tf.placeholder("float", [vocab_size],name='BM')
    Mask=tf.placeholder("float", [vocab_size],name='Mask')

    tf.Variable(vocab_size,name='vocab_size')
    tf.Variable(maxSeqlength,name='maxSeqlength')
    tf.Variable(n_hidden,name='n_hidden')


    alpha=tf.Variable(0.5,dtype=tf.float32, name='alpha')
    beta=tf.Variable(0.5,dtype=tf.float32, name='beta')

    with open('../Models/'+Influenced+'/dictionary.json', 'w') as fp:
        json.dump(dictionary, fp)
    with open('../Models/'+Influenced+'/reverse_dictionary.json', 'w') as fp:
        json.dump(reverse_dictionary, fp)



    # RNN output node weights and biases
    weights = {
        'out_rnn': tf.Variable(tf.random_normal([n_hidden, vocab_size]),name='weights_out_rnn'),
        'h1_Topic': tf.Variable(tf.random_normal([vocab_size,n_hidden_1_MLP]),name='weights_h1_Topic'),
        #'h2_Topic': tf.Variable(tf.random_normal([n_hidden_1_MLP, n_hidden_2_MLP]),name='weights_h2_Topic'),
        'out_Topic_PI': tf.Variable(tf.random_normal([n_hidden_2_MLP,n_topics]),name='weights_out_Topic_PI'),
        #'out_Topic': tf.Variable(tf.random_normal([vocab_size, n_topics]),name='weights_out_Topic'),
        #'alpha_RNN': tf.Variable(tf.random_normal([1]),name='alpha_RNN'),
        #'alpha_Topic': tf.Variable(tf.random_normal([1]),name='alpha_Topic'),
        #'alpha_History': tf.Variable(tf.random_normal([1]),name='alpha_History'),
        #'lambda_Topic': tf.Variable(tf.random_normal([1]),name='lambda_Topic'),
        #'lambda_B': tf.Variable(tf.random_normal([1]),name='lambda_B'),
        'h1_History': tf.Variable(tf.random_normal([r*n_topics,n_hidden_1_MLP])),
        'h1_Influential_History': tf.Variable(tf.random_normal([r*n_topics,n_hidden_1_MLP])),
        #'h2_History': tf.Variable(tf.random_normal([n_hidden_1_MLP, n_hidden_2_MLP]),name='weights_h2_History'),
        #'VAR_History': tf.Variable(tf.random_normal([n_hidden_1_MLP, vocab_size]),name='weights_VAR_History'),
        'out_History': tf.Variable(tf.random_normal([n_hidden_2_MLP, vocab_size])),
        'out_Influential_History': tf.Variable(tf.random_normal([n_hidden_2_MLP, vocab_size])),
        #'h_Joint': tf.Variable(tf.random_normal([vocab_size*2,n_hidden_Joint]),name='weights_h_Joint'),
        #'out_Joint': tf.Variable(tf.random_normal([n_hidden_Joint,vocab_size]),name='weights_out_Joint')
    }
    biases = {
        'out_rnn': tf.Variable(tf.random_normal([vocab_size]),name='biases_out_rnn'),
        'b1_Topic': tf.Variable(tf.random_normal([n_hidden_1_MLP]),name='biases_b1_Topic'),
        #'b2_Topic': tf.Variable(tf.random_normal([n_hidden_2_MLP]),name='biases_b2_Topic'),
        'out_Topic_PI': tf.Variable(tf.random_normal([n_topics]),name='biases_out_Topic_PI'),
        #'out_Topic': tf.Variable(tf.random_normal([vocab_size]),name='biases_out_Topic'),
        'b1_History': tf.Variable(tf.random_normal([n_hidden_1_MLP])),
        'b1_Influential_History': tf.Variable(tf.random_normal([n_hidden_1_MLP])),
        #'b2_History': tf.Variable(tf.random_normal([n_hidden_2_MLP]),name='biases_b2_History'),
        'out_History': tf.Variable(tf.random_normal([vocab_size])),
        'out_Influential_History': tf.Variable(tf.random_normal([vocab_size])),
        #'out_History': tf.Variable(tf.random_normal([n_topics]),name='biases_out_History'),
        #'b_Joint': tf.Variable(tf.random_normal([n_hidden_Joint]),name='biases_b_Joint'),
        #'out_Joint': tf.Variable(tf.random_normal([vocab_size]),name='biases_out_Joint')
    }


    #pred = tf.identity(tf.add(dynamicRNN(x,seqlen,weights, biases),tf.add(Topic_network(seqBagofWords, weights, biases), History_network(seqyear, weights, biases))), name='pred')
    pred = tf.identity(JointNetwork(x, seqlen, weights, biases, seqyear, alpha, beta), name='pred')
    HistoryNet = tf.identity(History_network(seqyear, weights, biases), name='HistoryNet')
    #TopicNet=tf.identity(Topic_network(seqBagofWords, weights, biases,HistoryNet),name='TopicNet')

    # Loss and optimizer
    cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=pred, labels=y),name='cost')
    optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)

    # Model evaluation
    correct_pred = tf.equal(tf.argmax(pred,1),tf.argmax(y,1),name='correct_pred')
    accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32),name='accuracy')

    # Initializing the variables
    init = tf.global_variables_initializer()

    
    with tf.Session() as sess:
        sess.run(init)
        sess.run(embedding_init, feed_dict={embedding_placeholder: embeddingsArray})
        saver = tf.train.Saver()
        step = 1
        # Keep training until reach max iterations
        while step <= training_iters:
            batch_x, batch_y, batch_seqlen, batch_year, batch_id = training_data.next(batch_size,Conference, YearToi, embeddingsDict, embedding_dim, r, maxSeqlength)
            # Run optimization op (backprop)
            sess.run(optimizer, feed_dict={x: batch_x, y: batch_y,
                                           seqlen: batch_seqlen, seqyear: batch_year})

            cp = sess.run(correct_pred, feed_dict={x: batch_x, y: batch_y,
                                                    seqlen: batch_seqlen, seqyear: batch_year})

            mypred = sess.run(pred, feed_dict={x: batch_x, y: batch_y,
                                                    seqlen: batch_seqlen, seqyear: batch_year})

            
                
            if step % display_step == 0:
                # Calculate batch accuracy
                acc = sess.run(accuracy, feed_dict={x: batch_x, y: batch_y,
                                                    seqlen: batch_seqlen, seqyear: batch_year})
                    
                # Calculate batch loss
                loss = sess.run(cost, feed_dict={x: batch_x, y: batch_y,
                                                 seqlen: batch_seqlen, seqyear: batch_year})
                '''print("Iter " + str(step) + ", Minibatch Loss= " + \
                      "{:.6f}".format(loss) + ", Training Accuracy= " + \
                      "{:.5f}".format((acc)))'''

                #print('alpha='+"{:.5f}".format(alpha.eval())+'\tbeta='+"{:.5f}".format(beta.eval()))
                #print(sess.run(TopicFunction, feed_dict={seqBagofWords: batch_seqBagofWords}).tolist()[0][:10])
                
            if step % model_save_step == 0:
                save_path = saver.save(sess, "../Models/"+Influenced+"/model.ckpt")
                #print("\n#############\nModel saved in file: %s\n\n" % save_path)
                #print('alpha_RNN: '+str(weights['alpha_RNN'].eval(session=sess))+'\talpha_Topic: '+str(weights['alpha_Topic'].eval(session=sess))+'\talpha_History: '+str(weights['alpha_History'].eval(session=sess)))
                #print('lambda_Topic: '+str(weights['lambda_Topic'].eval(session=sess))+'\tlambda_B: '+str(weights['lambda_B'].eval(session=sess)))

                #MyBackGroundModel=biases['out_Topic'].eval(session=sess).tolist()
                #MyBackGroundModel=biases['out_Topic'].eval(session=sess).tolist()
                '''TopicModel=[]

                for word in sorted(dictionary, key=dictionary.get):
                    frCount=np.zeros([vocab_size], dtype=float)
                    frCount[dictionary[word]]=1.0
                    TopicModel.append(sess.run(TopicNet,feed_dict={seqBagofWords: [frCount], seqyear: [YearToi[19]]}).flatten().tolist())

                #print(len(TopicModel),len(TopicModel[0]))
                TopicModel=numpy.asarray(TopicModel).transpose().tolist()
                #print(len(TopicModel),len(TopicModel[0]))

                with open('../Models/BM.txt','w') as outputfile:
                    for i in range(len(MyBackGroundModel)):
                        outputfile.write(str(MyBackGroundModel[i]))
                        if i<len(MyBackGroundModel)-1:
                            outputfile.write(',')
                        
                with open('TrainTopicModel.txt','w') as outputfile:
                    outputfile.write('BackGround Topic\n-------------------------------\n')
                    for j in sorted(range(len(MyBackGroundModel)), key=lambda k: MyBackGroundModel[k], reverse=True)[:20]:
                        outputfile.write(reverse_dictionary[j]+': '+str(j)+': '+ str(MyBackGroundModel[j])+'\n')

                    outputfile.write('\n\n\n\n')
                    
                    for i in range(len(TopicModel)):
                        outputfile.write('Topic No: '+str(i+1)+'\n-------------------------------\n')
                        for j in sorted(range(len(TopicModel[i])), key=lambda k: TopicModel[i][k], reverse=True)[:10]:
                            outputfile.write(reverse_dictionary[j]+': '+str(TopicModel[i][j])+'\n')
                        outputfile.write('\n\n\n\n')'''

                        

                predictedNumberofPaper=1
                with open('output.txt','w') as outputfile:
                    for confname in confs:
                        PredictedTitles=[]
                        #print('\n\n\nProducing papers for Conference: '+confname+'\n-----------------------------------------\n')
                        #outputfile.write('\n\n\nProducing papers for Conference: '+confname+'\n-----------------------------------------\n')
                        
                        conf=confname.replace('.txt','')
                        for i in range(predictedNumberofPaper):
                            while(True):
                                sentence=[]
                                sentence.append([])
                                sentence[0].append(dictionary['#'])
                                #sentence[0]+=[[0.0 for m in range(embedding_dim)] for j in range(maxSeqlength - 2)]
                                sentence[0]+=[dictionary['$emp$'] for Myi in range(maxSeqlength - 1)]
                                s='# '

                                temparatue=0.02
                                
                                for j in range(1,maxSeqlength):
                                    #print('shape:' +str(np.shape(sentence)))

                                    frCount=np.zeros([vocab_size], dtype=float)
                                    Mycount = collections.Counter(s.split()[0:j]).most_common()
                                    for word,wordcount in Mycount:
                                        frCount[dictionary[word]]=float(wordcount)

                                    MySum=sum(frCount)
                                    if MySum!=0:
                                        frCount=frCount/MySum
                            
                                    onehot_pred = sess.run(pred, feed_dict={x: sentence, seqlen: [j], seqyear: [YearToi[19]], seqBagofWords: [frCount] })

                                    Minimum=min(onehot_pred[0])
                                    mysum=0.0
                                    if Minimum<0:
                                        for k in range(len(onehot_pred[0])):
                                            onehot_pred[0][k]+=-Minimum+1
                                            mysum+=onehot_pred[0][k]
                                    k=0
                                    multinomial=[]
                                    for k in range(len(onehot_pred[0])):
                                        multinomial.append(onehot_pred[0][k]/mysum)
                                        k+=1

                                    for number in multinomial:
                                        if number <=0:
                                            print(number)

                                    onehot_pred_index=sample(multinomial,temparatue)
                                    temparatue=max(0.02,temparatue/2)


                                    '''cdf = [multinomial[0]]
                                    for k in range(1, len(multinomial)):
                                        cdf.append(cdf[-1] + multinomial[k])
                                    onehot_pred_index = bisect(cdf,random())'''

                                    onehot_pred_index = (int(tf.argmax(onehot_pred, 1).eval()))
                                    
                                    
                                    #sentence[0][j]=embeddingsDict[reverse_dictionary[str(onehot_pred_index)]]
                                    sentence[0][j]=dictionary[reverse_dictionary[(onehot_pred_index)]]
                                    #print(reverse_dictionary[onehot_pred_index])
                                    s+=reverse_dictionary[(onehot_pred_index)]+' '
                                    if reverse_dictionary[(onehot_pred_index)]=='.':
                                        break
                                if j>=1:
                                    break
                            outputfile.write(str(i+1)+' '+ str(len(s.split()))+' '+s+'\n\n')
                            #print(str(i+1)+': '+ s)
                        #print('\n\n')
            step += 1

            


        #print("Optimization Finished!")
        #print("Elapsed time: ", elapsed(time.time() - start_time))

        failures=np.where(cp == 0)[0]
        #for index in failures:
            #print(reverse_dictionary[np.argmax(mypred[index])], reverse_dictionary[np.argmax(batch_y[index])])

    tf.reset_default_graph()          


            
        
            



