import sys
import shutil
import os
from os import listdir
from os.path import isfile, join
import numpy as np
#from statsmodels.tsa.api import VAR, DynamicVAR
#import NDCG
#import RBO
from bisect import bisect
from random import random
from nltk.translate import bleu_score
from nltk.translate.bleu_score import SmoothingFunction
import codecs
import rougescore
import json





def isEnglish(s):
    try:
        codecs.encode(s,'ascii')
    except:
        return False
    else:
        return True




InfluencedList=['kdd','nips','sigir','icml','www','cvpr']

for Influenced in InfluencedList:
    print('Testing: '+ Influenced)
    DataDirectory='/datadrive/Santu/Influence/Data/'+Influenced
    #OutputDirectory='/home/t-shsant/Data/CleanData/'

    TestYear=18

    Stopwords=[]

    '''
    with open('stop-word-list.txt','r')as inputfile:
        for line in inputfile:
            Stopwords.append(line.strip().lower())'''
            

    def sample(preds, temperature=1.0):
        # helper function to sample an index from a probability array
        preds = np.asarray(preds).astype('float64')
        preds = np.log(preds) / temperature
        exp_preds = np.exp(preds)
        preds = exp_preds / np.sum(exp_preds)
        probas = np.random.multinomial(1, preds, 1)
        return np.argmax(probas)


    def remove_stopwords(sentence):
        cleanSentence=[]
        for word in sentence:
            if word not in Stopwords:
                cleanSentence.append(word)
        return cleanSentence


    with open('../Models/'+Influenced+'/dictionary.json', 'r') as fp:
        dictionary=json.load(fp)
    with open('../Models/'+Influenced+'/reverse_dictionary.json', 'r') as fp:
        reverse_dictionary=json.load(fp)

        
    confs=['TrainPapers.txt']

    for confname in confs:
        TotalPapers=0
        BigramCounts={}
        NumberofPapers={}
        ActualTitles={}
        conf=confname.replace('.txt','')
        MaxTitleLen=0
        UnigramMapping={}
        UnigramReverseMapping={}
        UnigramCounter=0

        
        with open(DataDirectory+'/'+conf+'.txt','r') as inputfile:
            for line in inputfile:
                TotalPapers+=1

                year=int(line.split('\t')[0])

                if year > TestYear:
                    continue

                InitialTitle=line.strip().split('\t')[1]
                if isEnglish(InitialTitle)==False:
                    continue
                

                Title=''
                for word in InitialTitle.split():
                    if word in dictionary:
                        Title+=word+' '

                Title='# ' + Title + ' .'

                TitleWords=Title.split()
                BigramSet=[]
                for i in range(len(TitleWords)-1):
                    BigramSet.append(TitleWords[i]+' '+TitleWords[i+1])
                BigramSet=list(set(BigramSet))

                
                if year <= TestYear:
                    for bigram in BigramSet:
                        if bigram not in BigramCounts:
                            BigramCounts[bigram]=1
                        else:
                            BigramCounts[bigram]+=1

                if year not in NumberofPapers:
                    NumberofPapers[year]=1
                else:
                    NumberofPapers[year]+=1

                


                if year not in ActualTitles:
                    ActualTitles[year]=[]
                ActualTitles[year].append(InitialTitle.split())

                if len(TitleWords) > MaxTitleLen:
                    MaxTitleLen=len(TitleWords)


                for word in TitleWords:
                    if word not in UnigramMapping:
                        UnigramMapping[word]=UnigramCounter
                        UnigramReverseMapping[UnigramCounter]=word
                        UnigramCounter+=1



        predictions={}
        itobigram={}

        mysum=0.0
        for bigram in BigramCounts:
            mysum+=BigramCounts[bigram]

        print('Computing Bigram Distribution')
        print('Total Bigrams: '+str(len(BigramCounts)))
        
        i=0
        multinomial=[]
        for bigram in sorted(BigramCounts):
            itobigram[i]=bigram
            multinomial.append(BigramCounts[bigram]/mysum)
            #multinomial.append(BigramCounts[bigram])
            i+=1

        i=0
        multinomial_conditional={}
        for bigram in sorted(BigramCounts):
            first_word=bigram.split()[0]
            second_word=bigram.split()[1]

            if first_word not in multinomial_conditional:
                multinomial_conditional[first_word]=[]
                for j in range(len(UnigramReverseMapping)):
                    multinomial_conditional[first_word].append(0.0)
            multinomial_conditional[first_word][UnigramMapping[second_word]]=(BigramCounts[bigram])
            i+=1


        for first_word in multinomial_conditional:
            mysum=sum(multinomial_conditional[first_word])
            for i in range(len(multinomial_conditional[first_word])):
                multinomial_conditional[first_word][i]/=mysum

        
        
        cdf = [multinomial[0]]
        for i in range(1, len(multinomial)):
            cdf.append(cdf[-1] + multinomial[i])

        cdf_conditional={}
        for first_word in multinomial_conditional:
            cdf_conditional[first_word] = [multinomial_conditional[first_word][0]]
            for i in range(1, len(multinomial_conditional[first_word])):
                cdf_conditional[first_word].append(cdf_conditional[first_word][-1] + multinomial_conditional[first_word][i])

        print('Ashlam')



        ActualTitlesTest={}
        with open(DataDirectory+'/TrainPapers.txt','r') as inputfile:
            for line in inputfile:
                year=int(line.split('\t')[0])
                if year > 19:
                    continue
                InitialTitle=line.strip().split('\t')[1]
                if isEnglish(InitialTitle)==False:
                    continue

                Title='#'
                for word in InitialTitle.split():
                    if word in dictionary:
                        Title+=' '+word
                Title+=' .'
                #print(Title)
                if year not in ActualTitlesTest:
                    ActualTitlesTest[year]=[]
                ActualTitlesTest[year].append(Title.split())



        print('Max title length: '+str(MaxTitleLen))
        for year in range(4,20):
            RefereneTitles=ActualTitlesTest[year]

            #print(len(RefereneTitles))

            predictedNumberofPaper=min(50, len(RefereneTitles))
            BLEUscores=[]

            

            PredictedTitles=[]
            IndividualBLEU4={}
            IndividualBLEU3={}
            IndividualBLEU2={}
            IndividualROUGE={}

            
            PairWiseBLEU4={}
            PairWiseBLEU3={}
            PairWiseBLEU2={}
            PairWiseROUGE={}

            
            MostSimilarTitle={}

            #print('generating paper: '+ str(itoYear[TrainingEndTime+t]))

            GeneratedBigramDistribution={}

            for i in range(predictedNumberofPaper):
                while(True):
                    generatedTitle=[]
                    generatedTitle.append('#')
                    #print('Sampling from distribution')
                    for j in range(1,MaxTitleLen):
                        if generatedTitle[-1] in cdf_conditional:
                            selectedUnigram=UnigramReverseMapping[bisect(cdf_conditional[generatedTitle[-1]],random())]
                            #selectedUnigram=UnigramReverseMapping[sample(cdf_conditional[generatedTitle[-1]],0.02)]
                        else:
                            selectedUnigram=itobigram[bisect(cdf,random())].split()[0]
                            #selectedUnigram=itobigram[sample(cdf[generatedTitle[-1]],0.02)].split()[0]
                        generatedTitle.append(selectedUnigram)
                        #print(selectedUnigram)
                        if selectedUnigram=='.' or j>15:
                            break
                    if j>15:
                        break

                #print('Generated for year '+str(year)+': '+str(i) )

                PredictedTitles.append(generatedTitle)
                #print(generatedTitle[1:-1])
                chencherry = SmoothingFunction()

                PairWiseBLEU2[i]={}
                PairWiseBLEU3[i]={}
                PairWiseBLEU4[i]={}
                PairWiseROUGE[i]={}

                #print('Computing Blue')

                for j in range(len(RefereneTitles)):
                    PairWiseBLEU4[i][j]=bleu_score.sentence_bleu([RefereneTitles[j][1:-1]],generatedTitle[1:-1],weights=(0.25, 0.25, 0.25, 0.25),smoothing_function=chencherry.method2)
                    PairWiseBLEU3[i][j]=bleu_score.sentence_bleu([RefereneTitles[j][1:-1]],generatedTitle[1:-1],weights=(0.33, 0.33, 0.33, 0.00),smoothing_function=chencherry.method2)
                    PairWiseBLEU2[i][j]=bleu_score.sentence_bleu([RefereneTitles[j][1:-1]],generatedTitle[1:-1],weights=(0.5, 0.5, 0.00, 0.00),smoothing_function=chencherry.method2)
                    #PairWiseBLEU[i][j]=float(bleu_score.modified_precision([RefereneTitles[0][j]],s.split()[1:-1],3))
                    PairWiseROUGE[i][j]=rougescore.rouge_l(generatedTitle[1:-1],[RefereneTitles[j][1:-1]],0.5)


                MostSimilarTitleNo=sorted(PairWiseBLEU4[i],key=PairWiseBLEU4[i].get, reverse=True)[0]


                IndividualBLEU4[i]=PairWiseBLEU4[i][MostSimilarTitleNo]
                IndividualBLEU3[i]=PairWiseBLEU3[i][MostSimilarTitleNo]
                IndividualBLEU2[i]=PairWiseBLEU2[i][MostSimilarTitleNo]
                IndividualROUGE[i]=PairWiseROUGE[i][MostSimilarTitleNo]


                        
                MostSimilarTitle[i]=RefereneTitles[MostSimilarTitleNo]
                #print(IndividualBLEU[i])
                del RefereneTitles[MostSimilarTitleNo]
                #print('Computed Blue')
                

            #for titleNo in sorted(IndividualBLEU,key=IndividualBLEU.get, reverse=True)[:10]:
                #print(str(IndividualBLEU[titleNo])+'\n'+' '.join(PredictedTitles[titleNo][1:-1])+'\n\n'+' '.join(MostSimilarTitle[titleNo])+'\n---------------------------------\n\n')
                
            mybleu4=sum(IndividualBLEU4.values())/len(IndividualBLEU4)
            mybleu3=sum(IndividualBLEU3.values())/len(IndividualBLEU3)
            mybleu2=sum(IndividualBLEU2.values())/len(IndividualBLEU2)
            myrouge=sum(IndividualROUGE.values())/len(IndividualROUGE)

                    
            #BLEUscores.append(mybleu)
            #print('\n\niteration: '+str(iteration)+'\tBlue Score: '+str(mybleu))
                          
            '''chencherry = SmoothingFunction()
            x=(bleu_score.corpus_bleu(RefereneTitles,PredictedTitles,weights=(0.0, 0.0, 0.0, 1.00),smoothing_function=chencherry.method5))
            BLEUscores.append(x)'''

            

        
            '''print('Actual-----------------')
            for bigram in sorted(BigramCounts[itoYear[TrainingEndTime+t]],key=BigramCounts[itoYear[TrainingEndTime+t]].get,reverse=True)[:10]:
                print(bigram,BigramCounts[itoYear[TrainingEndTime+t]][bigram])

            print('Predicted-----------------')
            for bigram in sorted(GeneratedBigramDistribution,key=GeneratedBigramDistribution.get,reverse=True)[:20]:
                print(bigram,GeneratedBigramDistribution[bigram])
            print('\n\n')

            RankedList=[]
            for mytuple in sorted(GeneratedBigramDistribution.items(),key=lambda x: (x[1],x[0]),reverse=True):
                RankedList.append(mytuple[0])

            for index in sorted(range(len(multinomial)),key=lambda x:multinomial[x],reverse=True):
                RankedList.append(itobigram[index])


            IdealList=[]
            for mytuple in sorted(BigramCounts[itoYear[TrainingEndTime+t]].items(),key=lambda x: (x[1],x[0]),reverse=True):
                IdealList.append(mytuple[0])

            #print(itoYear[TrainingEndTime+t],NDCG.computeNDCGatK(RankedList,IdealList,10), RBO.computeRBOatK(RankedList,IdealList,10))
            #NDCGscores.append(NDCG.computeNDCGatK(RankedList,IdealList,20))
            #RBOscores.append(RBO.computeRBOatK(RankedList,IdealList,20))
            #print(itoYear[TrainingEndTime+t],iteration,x)'''
            

            print(str(mybleu2)+','+str(mybleu3)+','+str(mybleu4)+','+str(myrouge))
        print('\n\n\n')


















        
        
