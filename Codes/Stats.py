


DataDirectory='/Users/shubhrakanti/Documents/DTG/Data/'


Conferences=['kdd','nips','sigir','icml','www','cvpr']

titleCountTotal=0
WordCountTotal=0
for conf in Conferences:
    titleCount=0
    WordCount=0
    with open (DataDirectory+conf+'.dat', encoding='utf-8') as inputfile:
        for line in inputfile:
            titleCount+=1
            WordCount+=len(line.split('\t')[1].split())

    titleCountTotal+=titleCount
    WordCountTotal+=WordCount
    
            
    print(titleCount, titleCount/float(20), WordCount, WordCount/float(titleCount))

print(titleCountTotal, WordCountTotal)
