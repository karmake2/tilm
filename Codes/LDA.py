import metapy
import pandas as pd
import sys
import seaborn as sns
import matplotlib.pyplot as plt
import shutil
import os
import numpy as np
import math
from scipy import interpolate
import datetime


metapy.log_to_stderr()




InputDirectory='/Users/shubhrakanti/Documents/DTG/Data/'
OutputDirectory='/Users/shubhrakanti/Documents/DTG/TopicData/'
FigureDirectory='/Users/shubhrakanti/Documents/DTG/Figures/'

#Influential='all'
#Influenced='kdd'

InfluenceFlag='icml'

try:
    shutil.rmtree(OutputDirectory)
except:
    print('Output Directory not present')

try:
    shutil.rmtree(FigureDirectory)
except:
    print('Figure Directory not present')
    
os.mkdir(OutputDirectory)
os.mkdir(FigureDirectory)
os.mkdir(FigureDirectory+'TopicTrends')
os.mkdir(FigureDirectory+'SwarmPlots')
os.mkdir(FigureDirectory+'PaperCountDistribution')


conferences=['sigir','kdd','icml']
with open(InputDirectory+'pairdata/pair/pair.dat.labels','w',encoding='utf-8') as outputfile2:
    with open(InputDirectory+'pairdata/pair/pair.dat','w',encoding='utf-8') as outputfile:
        for conf in conferences:
            with open(InputDirectory+conf+'.dat','r',encoding='utf-8') as inputfile:
                for line in inputfile:
                    outputfile.write(line.split('\t')[1].strip()+'\n')
                    outputfile2.write(conf+':'+line.split('\t')[0].strip()+'\n')



fidx = metapy.index.make_forward_index(InputDirectory+'pair.toml')
dset = metapy.classify.MulticlassDataset(fidx)

print(fidx.class_labels())


'''model = metapy.topics.LDAParallelGibbs(docs=dset, num_topics=15, alpha=0.1, beta=0.1)
model.run(num_iters=2000)
model.save(InputDirectory+'lda-pgibbs-pair')'''


model = metapy.topics.TopicModel(InputDirectory+'lda-pgibbs-pair')


with open(OutputDirectory+'topics.txt','w') as outputfile:
    for topic in range(0, model.num_topics()):
        outputfile.write("\n\n\nTopic {}:\n".format(topic + 1))
        for tid, val in model.top_k(topic, 20, metapy.topics.BLTermScorer(model)):
            outputfile.write(("{}: {}\n".format(fidx.term_text(tid), val)))
        outputfile.write("======\n")


data = []
#selectedYears=[InfluenceFlag+':1997', InfluenceFlag+':2000', InfluenceFlag+':2003', InfluenceFlag+':2006', InfluenceFlag+':2009', InfluenceFlag+':2012', InfluenceFlag+':2015']

TopicTrend={}
PaperCountDistributions={}


for i in range(0, model.num_topics()):
    TopicTrend[i+1]={}


selectedYears=[InfluenceFlag+':2000', InfluenceFlag+':2003', InfluenceFlag+':2006', InfluenceFlag+':2009', InfluenceFlag+':2012', InfluenceFlag+':2015']


for doc in dset:
    proportions = model.topic_distribution(doc.id)

    mylabel=dset.label(doc).split(':')[1]

    if int(mylabel) < 2000:
        continue
    if InfluenceFlag in dset.label(doc):
        if mylabel not in PaperCountDistributions:
            PaperCountDistributions[mylabel]=0
        PaperCountDistributions[mylabel]+=1
        
        for i in range(model.num_topics()):
            if mylabel not in TopicTrend[i+1]:
                TopicTrend[i+1][mylabel]=0.0
            TopicTrend[i+1][mylabel]+=proportions.probability(i)

    if dset.label(doc) in selectedYears:
        proportions = model.topic_distribution(doc.id)
        data.append([dset.label(doc)] + [proportions.probability(i) for i in range(0, model.num_topics())])
df = pd.DataFrame(data, columns=['label'] + ["Topic {}".format(i + 1) for i in range(0, model.num_topics())])



for topic in TopicTrend:
    for year in TopicTrend[topic]:
        TopicTrend[topic][year]/=PaperCountDistributions[year]


with open(OutputDirectory+'topicDistribution.'+InfluenceFlag+'.txt','w') as outputfile:
    for year in sorted(TopicTrend[1]):
        outputfile.write(str(int(year)-1996)+'=')
        for topic in TopicTrend:
            outputfile.write(str(topic)+':'+str(TopicTrend[topic][year])+',')
        outputfile.write('\n')          
        



for i in range(model.num_topics()):
    lists = sorted(TopicTrend[i+1].items()) # sorted by key, return a list of tuples

    x, y = zip(*lists) # unpack a list of pairs into two tuples
    
    x=list(map(int,x))
    x=list(map(str,x))

    plt.figure()
    ax = plt.subplot(111)
    ax.plot(x, y, 'b--',x, y, 'b*',markersize=12)
    ax.locator_params(integer=True)
    
    box = ax.get_position()
    ax.set_position([box.x0, box.y0 + box.height * 0.1, box.width, box.height * 0.9])

    axes = plt.gca()
    #axes.set_ylim([0.0,0.3])
    ax.tick_params(axis='both', which='major', labelsize=15)
    ax.tick_params(axis='both', which='minor', labelsize=15)
    
    plt.savefig(FigureDirectory+"TopicTrends/Topic-{}-".format(i + 1)+InfluenceFlag+'.png',bbox_inches='tight')
    plt.close()
    



lists = sorted(PaperCountDistributions.items()) # sorted by key, return a list of tuples

x, y = zip(*lists) # unpack a list of pairs into two tuples

    
y_pos = np.arange(len(x))
plt.bar(y_pos, y)
plt.xticks(y_pos, x)
plt.xticks(rotation=45)

plt.savefig(FigureDirectory+"PaperCountDistribution/"+InfluenceFlag+".png")
plt.close()

    
'''for i in range(0, model.num_topics()):
    print("Generating Figure for Topic {}".format(i + 1))
    sns.swarmplot(data=df, x='label', y="Topic {}".format(i + 1))
    axes = plt.gca()
    axes.set_ylim([0.3,1.0])
    plt.savefig(FigureDirectory+"SwarmPlots/Influential: Topic {}".format(i + 1)+'.png')
    plt.close()


data = []
selectedYears=[InfluenceFlag+':2000', InfluenceFlag+':2005', InfluenceFlag+':2010', InfluenceFlag+':2015']

for doc in dset:
    if dset.label(doc) in selectedYears:
        proportions = model.topic_distribution(doc.id)
        data.append([dset.label(doc).replace('Influenced:','')] + [proportions.probability(i) for i in range(0, model.num_topics())])
df = pd.DataFrame(data, columns=['label'] + ["Topic {}".format(i + 1) for i in range(0, model.num_topics())])


for i in range(0, model.num_topics()):
    print("Generating Figure for Topic {}".format(i + 1))
    sns.swarmplot(data=df, x='label', y="Topic {}".format(i + 1))
    plt.savefig(FigureDirectory+"SwarmPlots/Influenced: Topic {}".format(i + 1)+'.png')
    plt.close()


with open(OutputDirectory+Influential+'.txt','w',encoding='utf-8') as outputfile:
    with open(InputDirectory+Influential+'.dat','r',encoding='utf-8') as inputfile:
        for line in inputfile:
            doc = metapy.index.Document()         
            doc.content(line.strip())
            dvec = fidx.tokenize(doc)
            inferencer = metapy.topics.GibbsInferencer(InputDirectory+'lda-pgibbs-pair.phi.bin', alpha=0.1)
            props = inferencer.infer(dvec, max_iters=100, rng_seed=42)
            outputfile.write(line.strip()+'\t'+str(props)+'\n')'''


with open(OutputDirectory+InfluenceFlag+'.txt','w',encoding='utf-8') as outputfile:
    with open(InputDirectory+InfluenceFlag+'.dat','r',encoding='utf-8') as inputfile:
        for line in inputfile:
            #doc = metapy.index.Document()         
            #doc.content(line.strip())
            #dvec = fidx.tokenize(doc)
            #inferencer = metapy.topics.GibbsInferencer(InputDirectory+'lda-pgibbs-pair.phi.bin', alpha=0.1)
            #props = inferencer.infer(dvec, max_iters=100, rng_seed=42)
            #outputfile.write(line.strip()+'\t'+str(props)+'\n')
            outputfile.write(line.strip()+'\n')

            



    
    
