import sys
import shutil
import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='3'
from os import listdir
from os.path import isfile, join
import numpy
#import NDCG
#import RBO
from bisect import bisect
from random import random
from nltk.translate import bleu_score
from nltk.translate.bleu_score import SmoothingFunction
import numpy as np
import tensorflow as tf
from tensorflow.contrib import rnn
import random
from random import random
import collections
import time
import codecs
import json
import rougescore
import math
from sklearn.cluster import KMeans
from random import randint



tf.logging.set_verbosity(tf.logging.ERROR)


def isEnglish(s):
    try:
        codecs.encode(s,'ascii')
    except:
        return False
    else:
        return True


Infleuntial='icml'
InfluencedList=['sigir']

resultFile=open('Results.txt','a')

resultFile.write('\n\n'+sys.argv[1]+'\n---------------\n')

print('\nRunning: '+sys.argv[1])
for Influenced in InfluencedList:
    print('Testing: '+ Influenced)
    resultFile.write('\n\nProcessing: '+ Influenced+'\n\n')
    DataDirectory='/datadrive/Santu/Influence/Data/'+Influenced+'/'
    Stopwords=[]


    with open('stop-word-list.txt','r')as inputfile:
        for line in inputfile:
            Stopwords.append(line.strip().lower())
            

    def remove_stopwords(sentence):
        cleanSentence=[]
        for word in sentence:
            if word not in Stopwords:
                cleanSentence.append(word)
        return cleanSentence


    def sample(preds, temperature=1.0):
        # helper function to sample an index from a probability array
        preds = np.asarray(preds).astype('float64')
        preds = np.log(preds) / temperature
        exp_preds = np.exp(preds)
        preds = exp_preds / np.sum(exp_preds)
        probas = np.random.multinomial(1, preds, 1)
        return np.argmax(probas)



    def MySoftMax(preds):
        preds = np.asarray(preds).astype('float64')
        #preds = np.log(preds)
        exp_preds = np.exp(preds)
        if np.sum(exp_preds)==0:
            print('Problem Problem')
        preds = exp_preds / np.sum(exp_preds)
        #probas = np.random.multinomial(1, preds, 1)
        return preds




    r=4

    YearToi={}
    Conference={}
    iToYear={}



    with open('../Models/'+Influenced+'/dictionary.json', 'r') as fp:
        dictionary=json.load(fp)
    with open('../Models/'+Influenced+'/reverse_dictionary.json', 'r') as fp:
        reverse_dictionary=json.load(fp)

    '''BackGroundModel=[]
    with open('../Saved_Models/BM.txt','r') as inputfile:
        for number in inputfile.readline().split(','):
            BackGroundModel.append(float(number))'''



                    

    confs = ['TrainPapers.txt']
    Conference={}
    ActualTitles=[]
    paperCount=0
    maxSeqlength=0

    AllWords=[]
    BagOfWords={}
    WordCount={}

    ConfArray=[]
    ConfDistribution={}

    IDF={}

    for confname in sorted(confs):
        conf=confname.replace('.txt','')
        ConfArray.append(conf)
        with open(DataDirectory+'/'+conf+'.txt','r') as inputfile:
            for line in inputfile:
                year=int(line.split('\t')[0])

                if year > 19:
                    continue
                InitialTitle=line.strip().split('\t')[1]

                if isEnglish(InitialTitle)==False:
                    continue

                Title='#'

                for word in InitialTitle.split():
                    if word in dictionary:
                        Title+=' '+word
                    #else:
                        #Title+=' <unk_'+conf+'>'

                Title+=' .'

                if len(Title.split()) < 5:
                    continue
                
                for word in set(InitialTitle.split()):
                    if word in dictionary:
                        if word not in IDF:
                            IDF[word]=1
                        else:
                            IDF[word]+=1


                if year not in BagOfWords:
                    BagOfWords[year]={}

                if conf not in BagOfWords[year]:
                    BagOfWords[year][conf]={}

                for word in Title.split()[1:-1]:
                    if word in BagOfWords[year][conf]:
                        BagOfWords[year][conf][word]+=1
                    else:
                        BagOfWords[year][conf][word]=1

                if year < 4 or year> 19:
                    continue

                if year not in Conference:
                    Conference[year]=[]

                Conference[year].append(Title.split())

                if len(InitialTitle.split())+1 > maxSeqlength:
                    maxSeqlength=len(Title.split())+1

                paperCount+=1



    ConferenceOneHotVector={}
    for confname in sorted(confs):
        conf=confname.replace('.txt','')
        ConferenceOneHotVector[conf]=numpy.zeros(len(ConfArray))
        ConferenceOneHotVector[conf][ConfArray.index(conf)]=1.0


    with tf.Session() as sess:   
        saver = tf.train.import_meta_graph('../Models/'+Influenced+'/model.ckpt.meta')
        saver.restore(sess,tf.train.latest_checkpoint('../Models/'+Influenced+'/'))


        vocab_size=sess.run('vocab_size:0')
        maxSeqlength=sess.run('maxSeqlength:0')
        #n_hidden=sess.run('n_hidden:0')



        graph=tf.get_default_graph()

        x = graph.get_tensor_by_name("x:0")
        y = graph.get_tensor_by_name("y:0")
        BM = graph.get_tensor_by_name("BM:0")
        seqlen = graph.get_tensor_by_name("seqlen:0")
        seqyear = graph.get_tensor_by_name("seqyear:0")
        #seqBagofWords = graph.get_tensor_by_name("seqBagofWords:0")  
        
        '''with open('variables.txt','w')as outputfile:
            for i in graph.get_operations():
                outputfile.write(i.name+'\n')'''

                
        pred = graph.get_tensor_by_name("pred:0")
        cost = graph.get_tensor_by_name("cost:0")
        #TopicNet = graph.get_tensor_by_name("TopicNet:0")
        


        


        for word in IDF:
            IDF[word]=math.log(1+(float(paperCount)/IDF[word]))

        
        #embeddingsDict,embeddingsArray=loadGloVe(embedFile,dictionary,reverse_dictionary)
        #embedding_dim = len(embeddingsArray[0])

        YearCounter=0
        InfluentialHistoryData=[]
        InfluencedHistoryData=[]

        with open(DataDirectory+'topicDistribution.'+Infleuntial+'.txt','r') as inputfile:
            for line in inputfile:
                year=int(line.split('=')[0])
                YearToi[year]=YearCounter
                iToYear[YearCounter]=year

                InfluentialHistoryData.append([])
                distribution=line.split('=')[1].split(',')

                for item in distribution[:-1]:
                    InfluentialHistoryData[YearCounter].append(float(item.split(':')[1]))
                YearCounter+=1

        #print(InfluentialHistoryData[0])

        YearCounter=0
        with open(DataDirectory+'topicDistribution.'+Influenced+'.txt','r') as inputfile:
            for line in inputfile:
                year=int(line.split('=')[0])
                YearToi[year]=YearCounter
                iToYear[YearCounter]=year

                InfluencedHistoryData.append([])
                distribution=line.split('=')[1].split(',')
                
                for item in distribution[:-1]:
                    InfluencedHistoryData[YearCounter].append(float(item.split(':')[1]))
                YearCounter+=1
                    

        #print(InfluencedHistoryData[0])

        AggregatedInfluentialHistoryData=[]
        AggregatedInfluencedHistoryData=[]

        for year in range(len(InfluentialHistoryData)):
            AggregatedInfluentialHistoryData.append([])
            for i in range(1,r+1):
                if year-i >=0:
                    for proportion in InfluentialHistoryData[year-i]:
                        AggregatedInfluentialHistoryData[year].append(proportion)
                else:
                    for proportion in InfluentialHistoryData[year-i]:
                        AggregatedInfluentialHistoryData[year].append(0.0)


        for year in range(len(InfluencedHistoryData)):
            AggregatedInfluencedHistoryData.append([])
            for i in range(1,r+1):
                if year-i >=0:
                    for proportion in InfluencedHistoryData[year-i]:
                        AggregatedInfluencedHistoryData[year].append(proportion)
                else:
                    for proportion in InfluencedHistoryData[year-i]:
                        AggregatedInfluencedHistoryData[year].append(0.0)


        '''HistoryData=[]

        i=0 
        for year in sorted(BagOfWords):
            YearToi[year]=i
            iToYear[i]=year
            for conf in ConfArray:
                HistoryData.append([])
                for index in range(1,r+1):
                    if i-index < 0:
                        for j in range(vocab_size):
                            HistoryData[i].append(0.0)
                    else:
                        if conf not in BagOfWords[iToYear[i-index]]:
                            for word in sorted(dictionary,key=dictionary.get):
                                HistoryData[i].append(0.0)
                        else:
                            for word in sorted(dictionary,key=dictionary.get):
                                if word in BagOfWords[iToYear[i-index]][conf]:
                                    MyTF=1+math.log(float(BagOfWords[iToYear[i-index]][conf][word]))
                                    HistoryData[i].append(MyTF*IDF[word])
                                else:
                                    HistoryData[i].append(0.0)
                i+=1

        
        for i in range(len(HistoryData)):
            for index in range(0,r):
                MySum=sum(HistoryData[i][vocab_size*index : vocab_size*(index+1)])
                if MySum==0.0:
                    continue
                for j in range(0,vocab_size):
                    HistoryData[i][vocab_size*index+j]=HistoryData[i][vocab_size*index+j]/MySum


        
        TopicModel=[]

        for word in sorted(dictionary, key=dictionary.get):
            frCount=np.zeros([vocab_size], dtype=float)
            frCount[dictionary[word]]=1.0
            TopicModel.append(sess.run(TopicNet,feed_dict={seqBagofWords: [frCount], seqyear: [YearToi[18]]}).flatten().tolist())

        #print(len(TopicModel),len(TopicModel[0]))
        TopicModel=numpy.asarray(TopicModel)

        kmeans = KMeans(n_clusters=25, random_state=15845).fit(TopicModel)

        Clusters={}
        Similarity={}

        i=0
        for topic in kmeans.labels_:
            if topic not in Clusters:
                Clusters[topic]=[]
            Clusters[topic].append(reverse_dictionary[str(i)])
            i+=1
            
        
        
                    
        IgnoreList=Stopwords+['<unk_ML>', '#', '.']

        topic=0
        for center in kmeans.cluster_centers_:
            Similarity[topic]={}
            for word in Clusters[topic]:
                Similarity[topic][word]=numpy.linalg.norm(TopicModel[dictionary[word]]-center)
            topic+=1

        topic=0
        with open('TopicModel.txt','w') as outputfile:
            for topic in Similarity:
                outputfile.write('Topic No: '+str(topic+1)+'\n-------------------------------\n')
                i=0
                for word in sorted(Similarity[topic],key=Similarity[topic].get)[:30]:
                    outputfile.write(word+' : '+str(Similarity[topic][word])+'\n')
                outputfile.write('\n\n\n')
                topic+=1



        TopicModel=TopicModel.transpose().tolist()
        

        
        with open('TopicModel.txt','w') as outputfile:

            for i in range(len(TopicModel)):
                outputfile.write('Topic No: '+str(i+1)+'\n-------------------------------\n')
                for j in sorted(range(len(TopicModel[i])), key=lambda k: TopicModel[i][k])[:30]:
                    outputfile.write(reverse_dictionary[str(j)]+': '+str(TopicModel[i][j])+'\n')
                outputfile.write('\n\n\n\n')


        TF_IDF={}
        for year in range(4,20):
            TF_IDF[year]={}
            for word in IDF:
                if word in BagOfWords[year][conf]:
                    TF_IDF[year][word]=(1+math.log(float(BagOfWords[year][conf][word]),2))*IDF[word]/100
                else:
                    TF_IDF[year][word]=0.0'''
                    
                
        
        '''with open('perplexity.txt','w') as outputfile:
            for confname in confs:
                Perplexity={}
                conf=confname.replace('.txt','')

                
                for year in range(4,5):
                    RefereneTitles=Conference[year]
                    individualYearPerplexity=[]

                    pp=0.0
                    wordCount=0
                    
                    
                    for i in range(len(RefereneTitles)):
                        sentence=[]
                        sentence.append(dictionary['#'])
                        sentence+=[dictionary['$emp$'] for Myi in range(maxSeqlength - 1)]

                        s='#'
                        for j in range(1,len(RefereneTitles[i])-1):
                            wordCount+=1
                            frCount=np.zeros([vocab_size], dtype=float)
                            Mycount = collections.Counter(s.split()[0:j]).most_common()

                            for word,wordcount in Mycount:
                                frCount[dictionary[word]]=float(wordcount)
                                MySum=sum(frCount)
                                
                            if MySum!=0:
                                frCount=frCount/MySum
                        
                            onehot_pred = sess.run(pred, feed_dict={x: [sentence], seqlen: [j], seqyear: [YearToi[year]], seqBagofWords: [frCount] })
                            #onehot_pred=(onehot_pred[0]-min(onehot_pred[0]))/(max(onehot_pred[0])-min(onehot_pred[0]))
                            #loss = sess.run(cost, feed_dict={x: [sentence], seqlen: [j], seqyear: [YearToi[year]], seqBagofWords: [frCount]})

                            #print(min(onehot_pred[0]),max(onehot_pred[0]),numpy.mean(onehot_pred[0]))

                            Minimum=min(onehot_pred[0])

                            if Minimum<0:
                                for k in range(len(onehot_pred[0])):
                                    onehot_pred[0][k]+=-Minimum

                            k=0
                            multinomial=[]
                            for k in range(len(onehot_pred[0])):
                                #multinomial.append(onehot_pred[0][k]*TF_IDF[year][reverse_dictionary[str(k)]])
                                multinomial.append(onehot_pred[0][k])

                                k+=1

                            
                            onehot_pred=MySoftMax(multinomial)
                            #print(onehot_pred[:200])

                            
                            
                            
                            #if onehot_pred[dictionary[RefereneTitles[i][j]]] >= 0.1:
                            #if BagOfWords[year][conf][RefereneTitles[i][j]] >= 50:
                            if TF_IDF[year][RefereneTitles[i][j]] >= 0.30:
                                #try:
                                pp+=-math.log(onehot_pred[dictionary[RefereneTitles[i][j]]],2)
                                #except:
                                #print(RefereneTitles[i][j],BagOfWords[year][conf][RefereneTitles[i][j]],TF_IDF[year][RefereneTitles[i][j]])
                            #print(RefereneTitles[i][j],onehot_pred[dictionary[RefereneTitles[i][j]]])
                                outputfile.write(RefereneTitles[i][j]+'\t'+str(onehot_pred[dictionary[RefereneTitles[i][j]]])+'\t'+str(BagOfWords[year][conf][RefereneTitles[i][j]])+'\t'+str(TF_IDF[year][RefereneTitles[i][j]])+'\n')
                            
                            sentence[j]=dictionary[RefereneTitles[i][j]]

                            s+=' '+RefereneTitles[i][j]
                            if RefereneTitles[i][j]=='.':
                                break
                            #print(RefereneTitles[i][j])
                        if i %100==0:
                            print(year,i,math.pow(2,pp/float(wordCount)))
                            
                    Perplexity[year]=math.pow(2,pp/float(wordCount))

        print('\n\n\n\nPrinting the final perplexity scores\n#################\n\n')
        for year in sorted(Perplexity):
            print(year,Perplexity[year])

        print('\n\n\n\n')'''
        
            
                
        

        #print('Predicted Number of Papers: '+str(predictedNumberofPaper))

        with open('output_'+Influenced+'.txt','w') as outputfile:
            for confname in confs:
                BLEUscores=[]
                #print('\n\n\nProducing papers for Conference: '+confname+'\n-----------------------------------------\n')
                #outputfile.write('\n\n\nProducing papers for Conference: '+confname+'\n-----------------------------------------\n')
                
                conf=confname.replace('.txt','')

                for year in list(map(int,YearToi))[4:-1]:
                    RefereneTitles=[]
                    PredictedTitles=[]
                    
                    IndividualBLEU4={}
                    IndividualBLEU3={}
                    IndividualBLEU2={}
                    IndividualROUGE={}
                    
                    PairWiseBLEU4={}
                    PairWiseBLEU3={}
                    PairWiseBLEU2={}
                    PairWiseROUGE={}
                    
                    MostSimilarTitle={}
                    
                    
                    RefereneTitles=Conference[YearToi[year]]

                    predictedNumberofPaper=min(50, len(RefereneTitles))
                    
                    for i in range(predictedNumberofPaper):
                        while(True):
                            sentence=[]
                            sentence.append([])
                            sentence[0].append(dictionary['#'])
                            #sentence[0].append(dictionary['Conf_'+conf+':'])
                            #sentence[0]+=[[0.0 for m in range(embedding_dim)] for j in range(maxSeqlength - 2)]
                            sentence[0]+=[dictionary['$emp$'] for Myi in range(maxSeqlength - 1)]
                            s=''

                            temparatue=0.02
                            
                            for j in range(1,maxSeqlength):
                                #print('shape:' +str(np.shape(sentence)))

                                frCount=np.zeros([vocab_size], dtype=float)
                                Mycount = collections.Counter(s.split()[0:j]).most_common()
                                for word,wordcount in Mycount:
                                    frCount[dictionary[word]]=float(wordcount)

                                MySum=sum(frCount)
                                if MySum!=0:
                                    frCount=frCount/MySum
                        
                                onehot_pred = sess.run(pred, feed_dict={x: sentence, seqlen: [j], seqyear: [YearToi[year]] })
                                


                                
                                Minimum=min(onehot_pred[0])
                                mysum=0.0
                                if Minimum<0:
                                    for k in range(len(onehot_pred[0])):
                                        onehot_pred[0][k]+=-Minimum+1
                                        mysum+=onehot_pred[0][k]
                                k=0
                                multinomial=[]
                                for k in range(len(onehot_pred[0])):
                                    multinomial.append(onehot_pred[0][k]/mysum)
                                    k+=1

                                for number in multinomial:
                                    if number <=0:
                                        print(number)

                                onehot_pred_index=sample(multinomial,temparatue)
                                temparatue=max(0.01,temparatue/2)


                                '''cdf = [multinomial[0]]
                                for k in range(1, len(multinomial)):
                                    cdf.append(cdf[-1] + multinomial[k])
                                onehot_pred_index = bisect(cdf,random())'''

                                #onehot_pred_index = int(tf.argmax(onehot_pred, 1).eval())
                                
                                
                                #sentence[0][j]=embeddingsDict[reverse_dictionary[str(onehot_pred_index)]]
                                sentence[0][j]=dictionary[reverse_dictionary[str(onehot_pred_index)]]
                                #print(reverse_dictionary[onehot_pred_index])
                                s+=reverse_dictionary[str(onehot_pred_index)]+' '
                                if reverse_dictionary[str(onehot_pred_index)]=='.':
                                    break
                            if j>=1:
                                break
                        outputfile.write(str(iToYear[year])+'\t'+s+'\n')
                        #print('iteration: '+str(iteration)+'\tgenerated sentence: '+str(i+1))
                        #print(str(year)+': '+str(i+1)+': '+ s)
                        PredictedTitles.append(s.split())


                        
                        chencherry = SmoothingFunction()
                        #IndividualBLEU[i]=rougescore.rouge_l(s.split(),RefereneTitles[0],0.5)
                        PairWiseBLEU2[i]={}
                        PairWiseBLEU3[i]={}
                        PairWiseBLEU4[i]={}
                        PairWiseROUGE[i]={}
                        #PairWiseRouge[i]={}

                        #print('Computing Blue')
                        #print(s.split())
                        #print(len(RefereneTitles))
                        for j in range(len(RefereneTitles)):
                            #print(len(RefereneTitles[j]))
                            PairWiseBLEU4[i][j]=bleu_score.sentence_bleu([RefereneTitles[j][1:-1]],s.split()[1:-1],weights=(0.25, 0.25, 0.25, 0.25),smoothing_function=chencherry.method2)
                            PairWiseBLEU3[i][j]=bleu_score.sentence_bleu([RefereneTitles[j][1:-1]],s.split()[1:-1],weights=(0.33, 0.33, 0.33, 0.00),smoothing_function=chencherry.method2)
                            PairWiseBLEU2[i][j]=bleu_score.sentence_bleu([RefereneTitles[j][1:-1]],s.split()[1:-1],weights=(0.5, 0.5, 0.00, 0.00),smoothing_function=chencherry.method2)
                            #PairWiseBLEU[i][j]=float(bleu_score.modified_precision([RefereneTitles[0][j]],s.split()[1:-1],3))
                            PairWiseROUGE[i][j]=rougescore.rouge_l(s.split()[1:-1],[RefereneTitles[j][1:-1]],0.5)
                        MostSimilarTitleNo=sorted(PairWiseBLEU4[i],key=PairWiseBLEU4[i].get, reverse=True)[0]

                        IndividualBLEU4[i]=PairWiseBLEU4[i][MostSimilarTitleNo]
                        IndividualBLEU3[i]=PairWiseBLEU3[i][MostSimilarTitleNo]
                        IndividualBLEU2[i]=PairWiseBLEU2[i][MostSimilarTitleNo]
                        IndividualROUGE[i]=PairWiseROUGE[i][MostSimilarTitleNo]
                        
                        #IndividualRouge[i]=PairWiseRouge[i][MostSimilarTitleNo]
                        MostSimilarTitle[i]=RefereneTitles[MostSimilarTitleNo]
                        del RefereneTitles[MostSimilarTitleNo]
                        #print('Computed Blue')


                    #for titleNo in sorted(IndividualBLEU,key=IndividualBLEU.get, reverse=True)[:5]:
                        #print(str(IndividualBLEU[titleNo])+'\n'+' '.join(PredictedTitles[titleNo])+'\n\n'+' '.join(MostSimilarTitle[titleNo])+'\n---------------------------------\n\n')

                    mybleu4=sum(IndividualBLEU4.values())/len(IndividualBLEU4)
                    mybleu3=sum(IndividualBLEU3.values())/len(IndividualBLEU3)
                    mybleu2=sum(IndividualBLEU2.values())/len(IndividualBLEU2)
                    myrouge=sum(IndividualROUGE.values())/len(IndividualROUGE)

                    
                    #BLEU4scores.append(mybleu4)
                    #BLEU4scores.append(mybleu4)
                    #BLEU4scores.append(mybleu4)
                    #BLEU4scores.append(mybleu4)
                    #print('year: '+str(year)+'\tBlue Score: '+str(mybleu))
                    print(str(mybleu2)+','+str(mybleu3)+','+str(mybleu4)+','+str(myrouge))
                    
                    
                #print(numpy.average(BLEUscores),numpy.std(BLEUscores))
    tf.reset_default_graph()




        



                    
